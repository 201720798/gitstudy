﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewCard", menuName = "Card")]
public class Card : ScriptableObject
{
    //0 for Common, 1 for Mage, 2 for Fighter, 3 for Gunner
    public int PlayerClass;

    //Attack Spell = 0, Buff Spell = 1, Debuff Spell = 2, Summon Spell = 3
    public int CardType;

    public int Cost;
    public int Health;
    public int Damage;
    public int Range;

    public string Name;
    public string Description;
}
