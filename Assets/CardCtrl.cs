﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardCtrl : MonoBehaviour {

    private int followflag = 0;
    private Vector3 velocity = Vector3.zero;

    //-100

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(followflag==1)
            transform.position = Vector3.SmoothDamp(transform.position, new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0), ref velocity, 0.15f);
    }

    public void Drag()
    {
        followflag = 1;
        //iTween.MoveUpdate(gameObject, iTween.Hash("x", Input.mousePosition.x, "y", Input.mousePosition.y, "time", 0.1f));
        //transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        return;
    }

    public void Drop()
    {
        followflag = 0;
        if (Input.mousePosition.y <= 150)
        {
            iTween.MoveTo(gameObject, iTween.Hash("x", 500f, "y", 50f, "time", 0.5f, "easetype", iTween.EaseType.easeOutExpo));
        }
        else
        {
            transform.position = Vector3.SmoothDamp(transform.position, new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0), ref velocity, 0.15f);
            iTween.FadeTo(gameObject, iTween.Hash("alpha", 0, "time", 1.0f));
            Debug.Log("Hi");
        }
        return;
    }
}
