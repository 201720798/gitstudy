﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardManager : MonoBehaviour
{

    public Card card;
    public Text Name;
    public Text Description;
    public Text Cost;
    public Text Health;
    public Text Damage;
    public Text Range;

    void Start()
    {
        Name.text = card.Name;
        Description.text = card.Description;
        Cost.text = card.Cost.ToString();
        Health.text = card.Health.ToString();
        Damage.text = card.Damage.ToString();
        Range.text = card.Range.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
