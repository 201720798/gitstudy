﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogCtrl : MonoBehaviour
{

    public Button Dialog;
    public Image Arrow;

    public float delay;
    public float Skip_delay;
    public int cnt;

    public string[] fulltext;
    public int dialog_cnt;
    string currentText;

    public bool text_exit;
    public bool text_full;
    public bool text_cut;

    void Start()
    {
        Get_Typing(dialog_cnt, fulltext);
    }

    void Update()
    {
        if (text_full == true)
        {
            Arrow.gameObject.SetActive(true);
        }
        if (text_exit == true)
        {
            Dialog.gameObject.SetActive(false);
        }
    }

    public void End_Typing()
    {
        if (text_full == true)
        {
            cnt++;
            text_full = false;
            text_cut = false;
            Arrow.gameObject.SetActive(false);
            StartCoroutine(ShowText(fulltext));
        }
        else
        {
            text_cut = true;
        }
    }

    public void Get_Typing(int _dialog_cnt, string[] _fullText)
    {
        text_exit = false;
        text_full = false;
        text_cut = false;
        cnt = 0;

        dialog_cnt = _dialog_cnt;
        fulltext = new string[dialog_cnt];
        fulltext = _fullText;

        StartCoroutine(ShowText(fulltext));
    }

    IEnumerator ShowText(string[] _fullText)
    {
        if (cnt >= dialog_cnt)
        {
            text_exit = true;
            StopCoroutine("showText");
        }
        else
        {
            currentText = "";
            for (int i = 0; i < _fullText[cnt].Length; i++)
            {
                if (text_cut == true)
                {
                    break;
                }
                currentText = _fullText[cnt].Substring(0, i + 1);
                this.GetComponent<Text>().text = currentText;
                yield return new WaitForSeconds(delay);
            }
            this.GetComponent<Text>().text = _fullText[cnt];
            yield return new WaitForSeconds(Skip_delay);

            text_full = true;
        }
    }
}
