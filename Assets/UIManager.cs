﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Slider HPBar;
    public Text HPText;
    public Image DialogBox;
    public Image PauseBox;
    public Image StatusBox;

    public float CurHP = 40; //public for test
    private float MaxHP = 50;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            PauseSet();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            StatusSet();
        }
        HPText.text = (int)CurHP + "/" + MaxHP;
        HPBar.value = CurHP / MaxHP;
    }

    public void Draw()
    {
        
    }

    public void PauseSet()
    {
        if(PauseBox.IsActive()==true)
        {
            PauseBox.gameObject.SetActive(false);
        }
        else
        {
            PauseBox.gameObject.SetActive(true);
        }
    }

    public void StatusSet()
    {
        if (StatusBox.IsActive() == true)
        {
            StatusBox.gameObject.SetActive(false);
        }
        else
        {
            StatusBox.gameObject.SetActive(true);
        }
    }

    public void DialogOn()
    {
        DialogBox.gameObject.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

}
